<?php

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['prefix'=>'user'], function () {
    Route::post('login','User\AuthController@login');
    Route::post('create','User\AuthController@create');
    Route::group(['middleware'=>'auth:sanctum'], function () {
        Route::post('rate-driver','User\AuthController@rateDriver');

    });
});
Route::group(['prefix'=>'driver', 'middleware'=>'auth:sanctum'], function () {
    Route::post('select-location','Driver\DriverController@selectLocation');
    Route::get('{location_id?}/all','Driver\DriverController@getlocationDrivers');
});
Route::group(['prefix'=>'trip', 'middleware'=>'auth:sanctum'], function () {
    Route::post('create','Quick_Trip\Driver\DriverController@createTrip');
    Route::post('delete','Quick_Trip\Driver\DriverController@destroy');
    Route::get('/all','Quick_Trip\Driver\DriverController@getTrips');
    Route::post('/available-drivers','Quick_Trip\User\UserController@getAvailableDrivers');
});

