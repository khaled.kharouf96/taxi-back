<?php

namespace App\Http\Controllers;

use App\Models\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
/**
 * @OA\Info(
 *    title="CUBE26 API Documentation",
 *    version="1.0.0",
 * )
 */
class ApiController extends Controller
{
    public function __construct()
    {
        $this->user = Auth::guard('sanctum')->user();
    }
    public function response($data=null,$message,$code){
        return response()->json([
          'data' => $data,
          'message' => $message,
          'code' => $code
        ],$code);
    }

}
