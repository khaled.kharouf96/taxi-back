<?php

namespace App\Http\Controllers\Api\V1\Quick_Trip\User;

use App\Models\Trip;
use App\Models\User;
use App\Models\Location;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;

class UserController extends ApiController
{
    public function getAvailableDrivers(Request $request){
         $request->validate([
            'source_id' => 'required',
            'destination_id' => 'required'
         ]);
         $trips =  Trip::with('driver')->whereSourceId($request->source_id)
                        ->whereDestinationId($request->destination_id)
                        ->where('end_at','>=',now())
                        ->get();
        $trips = $trips->map(function($trip){
            $user = User::with('driverProps')->findOrFail($trip->user_id);
            $driver = $user->driverProps;
            $driver =  [
              'name' =>  $user->name,
              'email' =>  $user->email,
              'phone'=>  $user->phone,
              'taxi_number' => $driver->taxi_number,
              'taxi_color' => $driver->taxi_color,
              'taxi_type' => $driver->taxi_type,
              'rating' => round(DB::table('driver_rate')->whereUserId($user->id)->get()->avg('rate')),
              'image' => $driver->image,
            ];
            $trip->source = Location::find($trip->source_id)->name;
            $trip->destination = Location::find($trip->destination_id)->name;
            $trip->driver_details = $driver;
            return $trip->makeHidden('source_id','destination_id','user_id','created_at','updated_at','driver');
        });
        if(count($trips)>0){
            $response = $this->response($trips,'success',200);
            return  $response;
        }
        $response = $this->response('','No trips found',404);
        return  $response;
    }
}
