<?php

namespace App\Http\Controllers\Api\V1\Quick_Trip\Driver;

use App\Models\Trip;
use App\Models\User;
use App\Models\Location;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;

class DriverController extends ApiController
{
    public function createTrip(Request $request)
    {
        $request->validate([
            'source_id' => 'required',
            'destination_id' => 'required',
            'start_at' => 'required',
            'end_at' => 'required|after:start_at',
            'price' => 'required',
        ]);
        $user = $this->user;
        if($user->type == User::DRIVER){
            $trip = new Trip;
            $trip->source_id= $request->source_id;
            $trip->destination_id= $request->destination_id;
            $trip->start_at= $request->start_at;
            $trip->end_at= $request->end_at;
            $trip->price= $request->price;
            $trip->user_id = $user->id;
            if( $trip->save()){
                $response = $this->response( $trip,'success',201);
                return  $response;
                }
                $response = $this->response('','Something went wrong',500);
                return  $response;
        }
        $response = $this->response('','Sorry you must be driver to get this kind of data',404);
        return  $response;
    }

    public function getTrips(Request $request){
        $user = $this->user->load('trips');
        $trips = $user->trips()->get()->map(function($trip){
            $trip->source= Location::find($trip->source_id)->name;
            $trip->destination = Location::find($trip->destination_id)->name;
            return $trip->makeHidden('source_id','destination_id','user_id','created_at','updated_at');
        });
        if(count($trips)>0){
            $response = $this->response( $trips,'success',200);
            return  $response;
            }
            $response = $this->response('','No trips found',404);
            return  $response;
    }

    public function destroy(Request $request){
        $request->validate([
            'trip_id' => 'required',
        ]);
        $user = $this->user;
        $trip= Trip::find($request->trip_id);
        if(!$trip){
            $response = $this->response('','No trip found',404);
            return  $response;
        }
        if( $trip->delete()){
            $response = $this->response( '','Deleted successfully',200);
            return  $response;
            }
            $response = $this->response('','Something went wrong',500);
            return  $response;
    }
}
