<?php

namespace App\Http\Controllers\Api\V1\Driver;

use App\Models\User;
use App\Models\Driver;
use App\Models\Location;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;

class DriverController extends ApiController
{
    public function selectLocation(Request $request){
        $request->validate([
            'location_id' => 'required',
            'status' => 'required'
        ]);
        $user =$this->user->load('driverProps');
        $location = Location::find($request->location_id);
        if(!$location){
            $response = $this->response('','No locations found',404);
            return  $response;
        }
        if($user->type == User::DRIVER){
            $driver = Driver::whereUserId($user->id)->first();
            $driver->status = $request->status;
            $driver->location_id = $location->id;
            $driver->save();
            $response = $this->response($driver,'success',200);
            return  $response;
        }
        $response = $this->response('','Sorry you must be driver to get this kind of data',404);
        return  $response;
    }

    public function getlocationDrivers(Request $request){
        $location_id = $request->location_id;
        $location = Location::with('drivers')->findOrFail($location_id);
        $drivers = $location->drivers()->get()->map(function($driver){
              $user = User::findOrFail($driver->user_id);
              return [
                'name' =>  $user->name,
                'email' =>  $user->email,
                'phone'=>  $user->phone,
                'taxi_number' => $driver->taxi_number,
                'taxi_color' => $driver->taxi_color,
                'taxi_type' => $driver->taxi_type,
                'rating' => round(DB::table('driver_rate')->whereUserId($user->id)->get()->avg('rate')),
                'image' => $driver->image,
              ];
        });
        if(count($drivers)>0){
            $response = $this->response($drivers,'success',200);
            return  $response;
        }
        $response = $this->response('','No drivers found',404);
        return  $response;
    }
    
    
}
