<?php

namespace App\Http\Controllers\Api\V1\User;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\ApiController;
use App\Models\Driver;
use App\Models\Rate;

class AuthController extends ApiController
{
    public function login(Request $request){
        $request->validate([
          'email'=>'required',
          'password' => 'required',
        ]);
        $device_name = 'web';
        $user = User::where('email', $request->email)->first();
        if (!$user || !Hash::check($request->password, $user->password)) {
            $response = $this->response('','Incorrect Login Details',401);
            return  $response;
        } else {
            $token = $user->createToken($device_name);
            $arrayToken = (array) $token;
                $arrayToken['type'] = $user->type;
                $token = (object) $arrayToken;
        }
        $response = $this->response($token,'success',200);
        return  $response;
    }

    public function create(Request $request){
        $request->validate([
           'name' => 'required',
           'email' => 'required|unique:users',
           'password' => 'required|confirmed',
           'phone' => 'required',
           'type'=> 'required',
        ]);
        if($request->type == User::DRIVER){
            $request->validate([
                'taxi_type' => 'required',
                'taxi_number'=> 'required',
                'taxi_color'=> 'required',
             ]); 
        }
        $name= $request->name;
        $email= $request->email;
        $password= Hash::make($request->password);
        $phone= $request->phone;
        $type= $request->type;

        $user = new User;
        $user->name =$name;
        $user->email =$email;
        $user->password =$password;
        $user->phone =$phone;
        $user->type =$type;
        if($user->save()){
            if($request->type == User::CUSTOMER){
                $response = $this->response($user,'success',201);
                return  $response;
            }
            else{
                $taxi_type = $request->taxi_type;
                $taxi_number = $request->taxi_number;
                $taxi_color = $request->taxi_color;
                $driver = new Driver;
                $driver->taxi_type =$taxi_type;
                $driver->taxi_number =$taxi_number;
                $driver->taxi_color =$taxi_color;
                if($request->has('image')){
                    $driver->image =$request->image;  
                }
                if( $user->driverProps()->save($driver)){
                    $response = $this->response($user->load("driverProps"),'success',201);
                    return  $response;
                    }
                    $response = $this->response('','Something went wrong',500);
                    return  $response;
            }
        }
        else{
            $response = $this->response('','Something went wrong',500);
            return  $response;
        }

    }
    public function rateDriver( Request $request){
            $request->validate([
                 'rate'=>'required',
                 'driver_id' => 'required'
            ]);
            $rate_number = $request->rate;
            $driver = User::find($request->driver_id);
            $user = $this->user;
            $rate = new Rate;
            $rate->rate = $rate_number;
            $rate->user_id = $user->id;
            $rate->driver_id = $driver->id;
            if( $rate->save()){
                $response = $this->response($rate,'success',200);
                return  $response;
            }
            $response = $this->response('','Something went wrong',500);
            return  $response;

    }
}
