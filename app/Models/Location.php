<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    use HasFactory;

    public function drivers(){
        return $this->hasMany(Driver::class)->whereStatus(Driver::ACTIVE);
    }
}
