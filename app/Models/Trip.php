<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    use HasFactory;
    protected $table = 'quick_trip';

    public function driver(){
        return $this->belongsTo(User::class,'user_id');
    }
}
