<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    use HasFactory;
    public $fillable =[
        'location_id',
        'status'
    ];
    protected $table = 'user_driver';
    const ACTIVE= 1;
    const UNACTIVE= 0;

    public function user(){
        $this->belongsTo(User::class,'user_id');
    }
}
