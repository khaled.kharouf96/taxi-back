<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    use HasFactory;

    public $fillable =[
        'driver_id',
        'rate'
    ];
    protected $table = 'driver_rate';
}
