<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\User;
use App\Models\Location;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
        // User::create([
        //     'name' => 'admin',
        //     'email' => 'admin@warehouse.com',
        //     'password' => Hash::make('password'), //password
        // ]);

        $file = file_get_contents(
            base_path() . "/locations.txt"
        );
        $filee = str_replace('"', '', $file);
        $array = explode(',',$file);

        foreach($array as $area){
            $location = new Location;
            $location->name = $area;
            $location->save();
        }
    }
}
