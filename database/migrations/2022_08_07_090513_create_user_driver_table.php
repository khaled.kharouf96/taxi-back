<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_driver', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('taxi_number')->nullable();
            $table->string('taxi_type')->nullable();
            $table->string('taxi_color')->nullable();  
            $table->string('image')->nullable();
            $table->unsignedBigInteger('location_id')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->timestamps();


            $table->foreign('location_id')->references('id')->on('locations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_driver');
    }
};
